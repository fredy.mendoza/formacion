﻿using System.Collections.Generic;
using VELOCICLI.Entidades;

namespace VELOCICLI.Validaciones
{
    public abstract class ValidarTransacciones
    {
        public abstract List<Error> Validar();

        public abstract void ValidarCantidadRegistros();

        public abstract void ValidarFechaEnRango(Transaccion transaccion, int linea);

        public abstract void ValidarFormatoFecha(Transaccion transaccion, int linea);

        public abstract void ValidarMonto(Transaccion transaccion, int linea);

        public abstract void ValidarRegistrosIncompletos(Transaccion transaccion, int linea);
    }
}
