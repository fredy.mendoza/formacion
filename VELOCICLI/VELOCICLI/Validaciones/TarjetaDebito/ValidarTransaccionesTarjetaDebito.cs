﻿using System;
using System.Collections.Generic;
using VELOCICLI.Entidades;

namespace VELOCICLI.Validaciones
{
    public class ValidarTransaccionesTarjetaDebito : ValidarTransacciones
    {
        protected List<Error> _errores;
        protected EncabezadoTransaccion _encabezadoTransaccion;
        protected List<Transaccion> _transacciones;

        public ValidarTransaccionesTarjetaDebito(List<Transaccion> transacciones, EncabezadoTransaccion encabezadoTransaccion, List<Error> errores)
        {
            _errores = errores;
            _encabezadoTransaccion = encabezadoTransaccion;
            _transacciones = transacciones;
        }
        public override List<Error> Validar()
        {
            int linea = 4;

            ValidarCantidadRegistros();

            _transacciones.ForEach(item =>
            {
                ValidarFechaEnRango(item, linea);
                ValidarFormatoFecha(item, linea);
                ValidarMonto(item, linea);
                ValidarRegistrosIncompletos(item, linea);
                linea++;
            });

            return _errores;
        }

        public override void ValidarCantidadRegistros()
        {
            if (_transacciones.Count != _encabezadoTransaccion.Tamano)
            {
                _errores.Add(new Error { MensajeError = "La cantidad de registros es diferente a la indicada en el tamaño del encabezado", LugarError = (int)LugarError.Encabezado, LineaError = 2 });
            }
        }

        public override void ValidarFechaEnRango(Transaccion transaccion, int linea)
        {
            if (Convert.ToDateTime(transaccion.FechaTransaccion) < _encabezadoTransaccion.FechaInicioPeriodo || Convert.ToDateTime(transaccion.FechaTransaccion) > _encabezadoTransaccion.FechaFinPeriodo)
            {
                _errores.Add(new Error { MensajeError = "La fecha de la transacción no está dentro del rango indicado en el encabezado", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
        }

        public override void ValidarFormatoFecha(Transaccion transaccion, int linea)
        {
            DateTime fecha;

            if (string.IsNullOrEmpty(transaccion.FechaTransaccion))
            {
                _errores.Add(new Error { MensajeError = "No se ingresó fecha", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
            else if (!DateTime.TryParseExact(transaccion.FechaTransaccion, _encabezadoTransaccion.Formato, null, System.Globalization.DateTimeStyles.None, out fecha))
            {
                _errores.Add(new Error { MensajeError = "La fecha de la transacción no tiene el formato indicado en el encabezado", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
        }

        public override void ValidarMonto(Transaccion transaccion, int linea)
        {
            decimal monto = 0;

            if (string.IsNullOrEmpty(transaccion.ValorTransaccion))
            {
                _errores.Add(new Error { MensajeError = "No se ingresó valor a la transacción", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
            else if (!decimal.TryParse(transaccion.ValorTransaccion.ToString(), out monto))
            {
                _errores.Add(new Error { MensajeError = "El valor de la transacción no puede contener caracteres especiales o símbolos", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
            else if (monto < 0)
            {
                _errores.Add(new Error { MensajeError = "El valor del monto no puede ser negativo", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
        }

        public override void ValidarRegistrosIncompletos(Transaccion transaccion, int linea)
        {
            if (string.IsNullOrEmpty(transaccion.CodigoProducto))
            {
                _errores.Add(new Error { MensajeError = "No se ingresó código de producto a la transacción", LugarError = (int)LugarError.Registro, LineaError = linea });
            }

            if (string.IsNullOrEmpty(transaccion.RazonSocial) && string.IsNullOrEmpty(transaccion.Usuario))
            {
                _errores.Add(new Error { MensajeError = "No se ingresó razón social o usuario a la transacción", LugarError = (int)LugarError.Registro, LineaError = linea });
            }

            if (string.IsNullOrEmpty(transaccion.TipoTransaccion))
            {
                _errores.Add(new Error { MensajeError = "No se ingresó tipo a la transacción", LugarError = (int)LugarError.Registro, LineaError = linea });
            }
        }
    }
}
