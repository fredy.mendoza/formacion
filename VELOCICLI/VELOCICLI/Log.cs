﻿using System;
using System.Collections.Generic;
using System.IO;

namespace VELOCICLI
{
    public static class Log
    {
        public static void Crear(List<string> datos, string periodo)
        {
            string mensaje = $"Fecha operación: {DateTime.Now}, Período: {periodo}, Análisis exitoso, Cantidad de archivos: {datos.Count}, Datos: {Environment.NewLine}{string.Join(Environment.NewLine, datos)}";
            GenerarArchivoPlano(mensaje);
        }

        public static void Crear(string datos, string periodo)
        {
            string mensaje = $"Fecha operación: {DateTime.Now}, Período: {periodo}, Análisis con errores, Mensaje(s) error: {Environment.NewLine}{datos}";
            GenerarArchivoPlano(mensaje);
        }

        public static void Crear(string mensaje)
        {
            mensaje = $"Fecha operación: {DateTime.Now}, Análisis con errores, Mensaje(s) error: {Environment.NewLine}{mensaje}";
            GenerarArchivoPlano(mensaje);
        }

        private static void GenerarArchivoPlano(string mensaje)
        {
            string path = @"D:\formacion\log.txt";

            if (!File.Exists(path))
            {
                using (StreamWriter archivo = File.CreateText(path))
                {
                    archivo.WriteLine(mensaje);
                }
            }
            else
            {
                StreamReader leerArchivo = new StreamReader(path);
                string valorArchivo = leerArchivo.ReadToEnd();
                leerArchivo.Close();

                StreamWriter escribirArchivo = new StreamWriter(path);
                escribirArchivo.WriteLine($"{valorArchivo}{Environment.NewLine}{mensaje}");
                escribirArchivo.Close();
            }
        }
    }
}
