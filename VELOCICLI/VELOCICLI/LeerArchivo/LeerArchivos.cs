﻿using System.Collections.Generic;

namespace VELOCICLI.LeerArchivo
{
    public abstract class LeerArchivos
    {
        public abstract List<string> ObtenerDatos(string rutaArchivo);
    }
}
