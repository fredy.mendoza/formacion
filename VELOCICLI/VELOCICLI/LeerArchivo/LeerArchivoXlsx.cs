﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VELOCICLI.LeerArchivo
{
    public class LeerArchivoXlsx : LeerArchivos
    {
        public override List<string> ObtenerDatos(string rutaArchivo)
        {
            List<string> datos = new List<string>();

            using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(rutaArchivo)))
            {
                var myWorksheet = xlPackage.Workbook.Worksheets.First();
                var totalFilas = myWorksheet.Dimension.End.Row;
                var totalColumnas = myWorksheet.Dimension.End.Column;

                for (int fila = 1; fila <= totalFilas; fila++)
                {
                    var row = myWorksheet.Cells[fila, 1, fila, totalColumnas].Where(x=> x.Value != null).Select(c => c.Value.ToString());
                    datos.Add(string.Join(";", row));
                }
            }

            return datos;
        }
    }
}
