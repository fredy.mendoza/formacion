﻿using System.Collections.Generic;
using VELOCICLI.LeerArchivo;

namespace VELOCICLI
{
    public class LeerArchivoCsv: LeerArchivos
    {
        public override List<string> ObtenerDatos(string rutaArchivo)
        {
            int contador = 0;
            string lineaArchivo;
            List<string> datosArchivo = new List<string>();
            
            System.IO.StreamReader file =
                new System.IO.StreamReader(rutaArchivo);

            while ((lineaArchivo = file.ReadLine()) != null)
            {
                datosArchivo.Add(lineaArchivo);
                 contador++;
            }

            file.Close();

            return datosArchivo;
        }
    }
}
