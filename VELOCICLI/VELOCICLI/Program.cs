﻿using System;
using System.Collections.Generic;
using System.Linq;
using VELOCICLI.CrearTransacciones;
using VELOCICLI.Entidades;
using VELOCICLI.LeerArchivo;
using VELOCICLI.Validaciones;

namespace VELOCICLI
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                bool existeExtension = true;
                CrearTransaccion transaccionTarjetaDebito = new CrearTransaccionTarjetaDebito();
                List<Transaccion> transacciones = new List<Transaccion>();
                EncabezadoTransaccion encabezadoTransaccion = new EncabezadoTransaccion();
                List<Error> errores = new List<Error>();
                LeerArchivos leerArchivo;

                string extensionArchivo = ExtensionArchivo();

                switch (extensionArchivo.ToLower())
                {
                    case "xlsx":
                        leerArchivo = new LeerArchivoXlsx();
                        break;
                    case "csv":
                        leerArchivo = new LeerArchivoCsv();
                        break;
                    default:
                        leerArchivo = null;
                        Console.WriteLine("La extensión ingresada no existe");
                        existeExtension = false;
                        break;
                }

                if (existeExtension)
                {
                    string rutaArchivo = @"D:\formacion\transaccion";
                    List<string> datosArchivo = leerArchivo.ObtenerDatos($"{rutaArchivo}.{extensionArchivo}");
                    datosArchivo = FormatoArchivo.AplicarFormato(datosArchivo);

                    if (datosArchivo != null && datosArchivo.Count > 0)
                    {
                        encabezadoTransaccion = CrearEncabezadoTransaccion.CrearEncabezado(datosArchivo);
                        transacciones = CrearListaDeTransacciones.Crear(datosArchivo, transaccionTarjetaDebito);
                        ValidarTransacciones validarTransacciones = new ValidarTransaccionesTarjetaDebito(transacciones, encabezadoTransaccion, errores);
                        errores = validarTransacciones.Validar();

                        ProcesarTransacciones(datosArchivo, transacciones, errores);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Log.Crear(ex.Message);
            }
        }

        private static void ProcesarTransacciones(List<string> datosArchivo, List<Transaccion> transacciones, List<Error> errores)
        {
            if (errores.Any())
            {
                string mensajeErrores = ControlErrores.CrearMensajeErrores(errores);
                Console.WriteLine(mensajeErrores);
                Log.Crear(mensajeErrores, datosArchivo.First().Split(':')[1]);
            }
            else
            {
                CalculosTransaccionTarjetaDebito calculosTransaccionTarjetaDebito = new CalculosTransaccionTarjetaDebito(transacciones);
                ImprimirCalculos(calculosTransaccionTarjetaDebito);
                string periodo = datosArchivo.First().Split(':')[1];
                datosArchivo.RemoveRange(0, 3);
                Log.Crear(datosArchivo, periodo);
            }
        }

        private static string ExtensionArchivo()
        {
            Console.WriteLine("Introduzca la extensión del archivo a leer");
            string extensionArchivo = Console.ReadLine();
            return !string.IsNullOrEmpty(extensionArchivo) ? extensionArchivo : "txt";
        }

        private static void ImprimirCalculos(CalculosTransaccionTarjetaDebito calculosTransaccionTarjetaDebito)
        {
            Console.WriteLine($"Total de ingresos durante el mes: {calculosTransaccionTarjetaDebito.TotalIngresosMes()}");
            Console.WriteLine($"Total de egresos durante el mes: {calculosTransaccionTarjetaDebito.TotalEgresosMes()}");
            Console.WriteLine($"Balance total del mes: {calculosTransaccionTarjetaDebito.BalanceTotalMes()}");
            Console.WriteLine($"Usuario con mayor cantidad de compras: {calculosTransaccionTarjetaDebito.UsuarioMayorCantidadCompras()}");
            Console.WriteLine($"Comercio al que se realizan la mayor cantidad de pedidos: {calculosTransaccionTarjetaDebito.ComercioMayorCantidadPedidos()}");
        }
    }
}

