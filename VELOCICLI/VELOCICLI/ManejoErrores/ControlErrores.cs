﻿using System;
using System.Collections.Generic;
using System.Text;
using VELOCICLI.Entidades;

namespace VELOCICLI
{
    public static class ControlErrores
    {
        public static string CrearMensajeErrores(List<Error> errores)
        {
            StringBuilder error = new StringBuilder();

            errores.ForEach(item =>
            {
                if (item.LugarError == (int)LugarError.Encabezado)
                {
                    error.Append($"ERROR EN ENCABEZADO: Mensaje Error: {item.MensajeError}{Environment.NewLine}");
                }
                else
                {
                    error.Append($"ERROR EN REGISTRO: Error ocurrido en la línea: {item.LineaError}, Mensaje Error: {item.MensajeError}{Environment.NewLine}");
                }
            });

            return error.ToString();
        }
    }
}
