﻿using System;
using System.Collections.Generic;
using System.Linq;
using VELOCICLI.Entidades;

namespace VELOCICLI
{
    public class CalculosTransaccionTarjetaDebito : CalculosTransacciones
    {
        private List<Transaccion> _transacciones;
        private decimal totalIngresos = 0;
        private decimal totalEgresos = 0;

        public CalculosTransaccionTarjetaDebito(List<Transaccion> transacciones)
        {
            _transacciones = transacciones;
        }

        public override decimal TotalIngresosMes()
        {
            List<Transaccion> ingresos = _transacciones.FindAll(x => x.TipoTransaccion.Equals("INGRESO"));

            ingresos.ForEach(item =>
            {
                totalIngresos += Convert.ToDecimal(item.ValorTransaccion);
            });

            return totalIngresos;
        }

        public override decimal TotalEgresosMes()
        {
            List<Transaccion> egresos = _transacciones.FindAll(x => x.TipoTransaccion.Equals("EGRESO"));

            egresos.ForEach(item =>
            {
                totalEgresos += Convert.ToDecimal(item.ValorTransaccion);
            });

            return totalEgresos;
        }

        public override decimal BalanceTotalMes()
        {
            return totalIngresos - totalEgresos;
        }

        public override string UsuarioMayorCantidadCompras()
        {
            var comprasUsuarios = ComprasUsuarios().OrderByDescending(x => x.CantidadCompras).ToList();

            if (comprasUsuarios[0].CantidadCompras > comprasUsuarios[1].CantidadCompras)
                return comprasUsuarios.Select(x => x.Usuario).First();
            else
                return "Todos los usuarios tienen la misma cantidad de compras";
        }

        private List<ComprasUsuarios> ComprasUsuarios()
        {
           List<ComprasUsuarios> comprasUsuarios = new List<ComprasUsuarios>();

            _transacciones.ForEach(item =>
            {
                if (!string.IsNullOrEmpty(item.Usuario)) {
                    ComprasUsuarios compraUsuario = comprasUsuarios.Find(x => x.Usuario.Equals(item.Usuario));

                    if (compraUsuario != null)
                    {
                        compraUsuario.CantidadCompras = compraUsuario.CantidadCompras + 1;
                    }
                    else
                    {
                        comprasUsuarios.Add(new ComprasUsuarios { Usuario = item.Usuario, CantidadCompras = 1 });
                    }
                }
            });
            return comprasUsuarios;
        }

        public override string ComercioMayorCantidadPedidos()
        {
            var pedidosComercios = PedidosComercios().OrderByDescending(x => x.CantidadPedidos).ToList();

            if (pedidosComercios[0].CantidadPedidos > pedidosComercios[1].CantidadPedidos)
                return pedidosComercios.Select(x => x.RazonSocial).First();
            else
                return "Todos los comercios tienen la misma cantidad de pedidos";
        }

        private List<PedidosComercios> PedidosComercios()
        {
            List<PedidosComercios> pedidosComercios = new List<PedidosComercios>();

            _transacciones.ForEach(item =>
            {
                if (!string.IsNullOrEmpty(item.RazonSocial))
                {
                    PedidosComercios pedidoComercio = pedidosComercios.Find(x => x.RazonSocial.Equals(item.RazonSocial));

                    if (pedidoComercio != null)
                    {
                        pedidoComercio.CantidadPedidos = pedidoComercio.CantidadPedidos + 1;
                    }
                    else
                    {
                        pedidosComercios.Add(new PedidosComercios { RazonSocial = item.RazonSocial, CantidadPedidos = 1 });
                    }
                }
            });
            return pedidosComercios;
        }
    }
}
