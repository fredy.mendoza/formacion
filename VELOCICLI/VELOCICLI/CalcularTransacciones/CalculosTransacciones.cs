﻿namespace VELOCICLI
{
    public abstract class CalculosTransacciones
    {
        public abstract decimal TotalIngresosMes();

        public abstract decimal TotalEgresosMes();

        public abstract decimal BalanceTotalMes();

        public abstract string UsuarioMayorCantidadCompras();

        public abstract string ComercioMayorCantidadPedidos();
    }
}
