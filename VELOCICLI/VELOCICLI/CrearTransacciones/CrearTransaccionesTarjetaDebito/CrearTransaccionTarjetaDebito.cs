﻿using System;
using VELOCICLI.Entidades;

namespace VELOCICLI
{
    public class CrearTransaccionTarjetaDebito : CrearTransaccion
    {
        public override Transaccion MapearDatosTransaccion(string registroArchivo, TransaccionBuilder builder)
        {
            int cedulaUsuario = 0;
            string[] datosIndividuales = registroArchivo.Split(';');

            builder.AddFechaTransaccion(datosIndividuales[0].ToString());
            builder.AddTipoTransaccion(datosIndividuales[1].ToString());

            if (int.TryParse(datosIndividuales[2].ToString(), out cedulaUsuario))
            {
                builder.AddUsuario(datosIndividuales[2].ToString());
            }
            else
            {
                builder.AddRazonSocial(datosIndividuales[2].ToString());
            }

            builder.AddCodigoProducto(datosIndividuales[3].ToString());
            builder.AddValorTransaccion(datosIndividuales[4]);

            return builder.transaccion;
        }
    }
}
