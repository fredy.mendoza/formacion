﻿using System.Collections.Generic;
using VELOCICLI.Entidades;

namespace VELOCICLI.CrearTransacciones
{
    public static class CrearListaDeTransacciones
    {
        public static List<Transaccion> Crear(List<string> datosArchivo, CrearTransaccion transaccionTarjetaDebito)
        {
            int contador = 0;
            Transaccion transaccion;
            TransaccionBuilder builder;
            List<Transaccion> listaTransacciones = new List<Transaccion>();

            datosArchivo.ForEach(item =>
            {
                if (contador > 2)
                {
                    transaccion = new Transaccion();
                    builder = new TransaccionBuilder(transaccion);

                    listaTransacciones.Add(transaccionTarjetaDebito.MapearDatosTransaccion(item, builder));
                }

                contador++;
            });

            return listaTransacciones;
        }
    }
}
