﻿using System;
using VELOCICLI.Entidades;

namespace VELOCICLI
{
    public class TransaccionBuilder
    {
        protected Transaccion _transaccion;

        public TransaccionBuilder(Transaccion transaccion)
        {
            _transaccion = transaccion;
        }

        public Transaccion transaccion
        {
            get { return _transaccion; }
        }

        public void AddFechaTransaccion(string fechaTransaccion)
        {
            _transaccion.FechaTransaccion = fechaTransaccion;
        }

        public void AddTipoTransaccion(string tipoTransaccion)
        {
            _transaccion.TipoTransaccion = tipoTransaccion;
        }

        public void AddUsuario(string usuario)
        {
            _transaccion.Usuario = usuario;
        }

        public void AddRazonSocial(string razonSocial)
        {
            _transaccion.RazonSocial = razonSocial;
        }

        public void AddCodigoProducto(string codigoProducto)
        {
            _transaccion.CodigoProducto = codigoProducto;
        }

        public void AddValorTransaccion(string valorTransaccion)
        {
            _transaccion.ValorTransaccion = valorTransaccion;
        }
    }
}
