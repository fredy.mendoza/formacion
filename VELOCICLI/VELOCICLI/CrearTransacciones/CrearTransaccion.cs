﻿using System.Collections.Generic;
using VELOCICLI.Entidades;

namespace VELOCICLI
{
    public abstract class CrearTransaccion
    {
       public abstract Transaccion MapearDatosTransaccion(string registroArchivo, TransaccionBuilder builder);
    }
}
