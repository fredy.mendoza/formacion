﻿using System;
using System.Collections.Generic;
using VELOCICLI.Entidades;

namespace VELOCICLI.CrearTransacciones
{
    public static class CrearEncabezadoTransaccion
    {
        private static EncabezadoTransaccion _encabezadoTransaccion = new EncabezadoTransaccion();

        public static EncabezadoTransaccion CrearEncabezado(List<string> datosArchivo)
        {
            int contador = 0;

            while (contador <= 2)
            {
                if (datosArchivo[contador].Contains("PERIODO"))
                {
                    CrearPeriodo(datosArchivo[contador]);
                }
                else if (datosArchivo[contador].Contains("TAMAÑO"))
                {
                    CrearTamano(datosArchivo[contador]);
                }
                else
                {
                    CrearFormato(datosArchivo[contador]);
                }

                contador++;
            }

            return _encabezadoTransaccion;
        }

        public static void CrearPeriodo(string registro)
        {
            string periodo = registro.Split(':')[1];
            var fechasPeriodo = periodo.Split('-');
            _encabezadoTransaccion.FechaInicioPeriodo = Convert.ToDateTime(fechasPeriodo[0]);
            _encabezadoTransaccion.FechaFinPeriodo = Convert.ToDateTime(fechasPeriodo[1]);
        }

        public static void CrearTamano(string registro)
        {
            _encabezadoTransaccion.Tamano = Convert.ToInt32(registro.Split(':')[1]);
        }

        public static void CrearFormato(string registro)
        {
            _encabezadoTransaccion.Formato = registro.Split(':')[1];
        }
    }
}
