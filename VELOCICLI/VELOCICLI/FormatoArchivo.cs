﻿using System.Collections.Generic;

namespace VELOCICLI
{
    public static class FormatoArchivo
    {
        public static List<string> AplicarFormato(List<string> datos)
        {
            List<string> datosFormateados = new List<string>();
            datos.ForEach(item =>
            {
                if (item.Split(';').Length == 2)
                {
                    datosFormateados.Add(item.Replace(';', ':'));
                }
                else
                {
                    datosFormateados.Add(item);
                }
            });

            return datosFormateados;
        }
    }
}
