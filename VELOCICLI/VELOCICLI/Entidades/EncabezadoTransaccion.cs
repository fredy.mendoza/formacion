﻿using System;

namespace VELOCICLI.Entidades
{
    public class EncabezadoTransaccion
    {
        public DateTime FechaInicioPeriodo { get; set; }

        public DateTime FechaFinPeriodo { get; set; }

        public int Tamano { get; set; }

        public string Formato { get; set; }
    }
}
