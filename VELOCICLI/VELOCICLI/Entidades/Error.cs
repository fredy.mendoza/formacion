﻿namespace VELOCICLI.Entidades
{
    public class Error
    {
        public string MensajeError { get; set; }

        public int LugarError { get; set; }

        public int LineaError { get; set; }
    }
}
