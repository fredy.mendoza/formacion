﻿using System;

namespace VELOCICLI.Entidades
{
    public class Transaccion
    {
        public string FechaTransaccion { get; set; }

        public string TipoTransaccion { get; set; }

        public string Usuario { get; set; }

        public string RazonSocial { get; set; }

        public string CodigoProducto { get; set; }

        public string ValorTransaccion { get; set; }
    }
}
